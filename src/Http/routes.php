<?php 

Route::post('captcha/check', function(){
	$rules = ['captcha' => 'required|captcha'];
	$validator = \Validator::make(request()->all(), $rules);
	if ($validator->fails()){
		return ['has_errors' => 1,'is_successful' => 0];
	}
	else
	{
		return ['has_errors' => 0, 'is_successful' => 1];
	}
});

Route::get('captcha/refresh', function(){
	return ['captcha_img' => captcha_img()];
});