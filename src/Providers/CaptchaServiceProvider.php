<?php

namespace Epigra\Captcha\Providers;

use Illuminate\Support\ServiceProvider;
use Mews\Captcha\CaptchaServiceProvider;

class CaptchaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerViews();
        $this->registerTranslations();
        $this->registerAssets();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(CaptchaServiceProvider::class);
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $this->loadViewsFrom(__DIR__.'/../Resources/views', 'captcha');

        $this->publishes([
            __DIR__.'/../Resources/views' => resource_path('views/vendor/captcha'),
        ], 'views');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/captcha');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'captcha');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'captcha');
        }
    }
    

    /**
     * Register assets.
     *
     * @return void
     */
    public function registerAssets()
    {
        $this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/captcha'),
        ], 'public');
    }
}
