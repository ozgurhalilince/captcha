

function refreshCaptcha(){
    $.ajax({
        url: "/captcha/refresh"
    }).done(function(newcaptcha) {
    $("#captcha_wrapper").html(newcaptcha.captcha_img);
    });
}


$('.has-captcha').children('input[type="submit"]')
                 .before('<div class="form-group">'+
                             '<div id="captcha_wrapper">{!! captcha_img() !!}</div>' +
                             '<input id="captcha" maxlength="5" size="5" type="text"' + 
                             'class="form-control form-control--style" placeholder="{{ trans("captcha.verification_code") }}"/>' +
                          '</div>')

$('.has-captcha').submit(function () {
    $.ajax({
      url: "/captcha/check",
      type: "POST",
      data: {captcha: $("#captcha").val() },
    })
      .done(function( data ) {
        if (data.is_successful == 1) {
            //
        }        
        refreshCaptcha();

      });
    return false;
    
});