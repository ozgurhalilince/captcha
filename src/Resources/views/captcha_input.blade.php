<div class="form-group">
	<div id="captcha_wrapper">{!! captcha_img() !!}</div>
	<input id="captcha" maxlength="5" size="5" type="text" class="form-control form-control--style" placeholder="{{ trans('captcha.verification_code') }}"/>
</div>