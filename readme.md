### Epigra Captcha

##### `"mews/captcha": "^2.1"` paketinin kurulumu yapılması gerekiyor.

app.php içerisinde 

```
    'providers' => [
        // ...
        Mews\Captcha\CaptchaServiceProvider::class,
    ]

    'aliases' => [
        // ...
        'Captcha' => Mews\Captcha\Facades\Captcha::class,
    ]
```

düzenlemelerini yapıyorz.

```
php artisan vendor:publish --provider=“Mews\Captcha\CaptchaServiceProvider” --tag=“config”
```
komutu ile config dosyası publish edilebilir.


Daha fazla ayrıntı için; https://github.com/mewebstudio/captcha 


##### Js dosyası (ve isteğe bağlı views klasörü) publish edilmeli

```
php artisan vendor:publish --provider="Epigra\Captcha\CaptchaServiceProvider" --tag="public"

php artisan vendor:publish --provider="Epigra\Captcha\CaptchaServiceProvider" --tag="views"
```

Js dosyası publish edildiği zaman Sayfadaki herhangi bir forma "has-captcha" class'ı eklendiğinde submit butonundan önce aşağıdaki captcha ekleniyor.

```
<div class="form-group">
   <div id="captcha_wrapper">{!! captcha_img() !!}</div>
   <input id="captcha" maxlength="5" size="5" type="text" class="form-control form-control--style" placeholder="Görsel Doğrulama Kodu"/>
</div>
```

Ayrıca captha_input view'unu da gerekli yerlerde include edebilirsiniz.